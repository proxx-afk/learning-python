lst = []
lst_note = []
lst_coef = []

#Demander le nombre de matière
nb_matiere = 0
while nb_matiere == 0:
    nb_matiere = int(input("Combien il y a de matière ? "))
    if nb_matiere <= 0:
        print("Tu dois saisir au moins une matière")
        print("")

#Saisi des matieres ainsi que le coef de chaque matière
i = 0
while i < nb_matiere:
    matieres = input("Saisi la matière n °" + str(i+1) + " : ")
    lst.append(matieres)
    coef = input("Qu'elle est le coef de " + str(lst[i]) + " : " )
    print("")
    lst_coef.append(coef)
    i +=1
print("")
    
print("Le programme va calculer la moyenne de ces matières : " + str(lst))

#Saisi des notes par matière
i = 0
for i in range (0,nb_matiere):
    notes = input("Qu'elle est ta note en " + str(lst[i] + " : "))
    lst_note.append(notes)

print("")

#Mathématique 
total = 0
i= 0
while i < len(lst_note):
    total = total + int(lst_note[i]) * int(lst_coef[i])
    i +=1


calcul_coef = 0
i = 0
while i < len(lst_coef):
    calcul_coef = calcul_coef + int(lst_coef[i])
    i +=1


moyenne = total / calcul_coef
print(" ")

print("Ta moyenne général est de : " + str(round(moyenne,2)) + "/20")
